angular.module('app.services', [])

.factory('homeservice', function ($http, $rootScope, global) {
    var p = {};

    p.getCart = function($rootScope) {
        global.startSpin();
        $.ajax({
            url: global.host+'/server.php',
            method: 'post',
            timeout: global.timeout,
            data: { action: 'getcart' },
            success: function(data) {
                console.log(JSON.parse(data));

                $rootScope.cart = JSON.parse(data);

                var cart = JSON.parse(data);
                var total = 0;
                var quant = 0;
                var price = 0;
                for(var i in cart){
                    price = parseInt(cart[i].price);
                    quant = parseInt(cart[i].quantity);
                    total += price * quant;
                    //update quantity ng-model for each cart item
                    $rootScope.cart[i].model = cart[i].quantity;
                }
                $rootScope.cart.total = total;

                $rootScope.$digest();
                global.stopSpin();
            },
            error: function(error) {
                console.log(2);
                console.log(error);
                global.stopSpin();
            }
        });
    }

    p.remove = function($rootScope, id) {

        global.startSpin();
        $.ajax({
            url: global.host+'/server.php',
            method: 'post',
            timeout: global.timeout,
            data: { action: 'remove', product_id: id },
            success: function(data) {
                console.log(data);

                $rootScope.cart = JSON.parse(data); 
                p.getCart($rootScope);

                global.stopSpin();
            },
            error: function(error) {
                console.log(2);
                console.log(error);
                global.stopSpin();
            }
        });
    }

    p.add = function($rootScope, $scope, id, q, bool) {
        global.startSpin();
        $.ajax({
            url: global.host+'/server.php',
            method: 'post',
            timeout: global.timeout,
            data: { action: 'addtocart', product_id: id, quantity: q, bool: bool },
            success: function(data) {
                console.log(data);

                $rootScope.cart = JSON.parse(data); 
                if( bool ) p.getProducts($scope);
                else if( !bool ) p.getCart($rootScope);
                
                global.stopSpin();
            },
            error: function(error) {
                console.log(2);
                console.log(error);
                global.stopSpin();
            }
        });
    }

    p.getProducts = function($scope) {
        global.startSpin();

        $.ajax({
            url: global.host+'/server.php',
            method: 'post',
            timeout: global.timeout,
            data: { action: 'products' },
            success: function(data) {
                console.log(JSON.parse(data));
                var products = JSON.parse(data);
                //console.log(products);

                $scope.products = products;
                $rootScope.$digest();

                global.stopSpin();
            },
            error: function(error) {
                console.log(2);
                console.log(error);
                global.stopSpin();
            }
        });
    }

    return p;
})

.factory('global', function($http, $rootScope, $timeout){
    return {
        //host: 'http://localhost/shoppingcart',
        host: 'http://93.188.166.194/shoppingcart',
        timeout: 10000,
        startSpin: function() {
            $timeout(function() {
                $rootScope.$loading = true;
            });
        },
        stopSpin: function() {
            $timeout(function() {
                $rootScope.$loading = false;
            });
        },
        init: function() {
            $rootScope.nav = true;
            $rootScope.quantities = [];
            //quantity limit on each purchase item
            var qs = 100;
            for(var i=0;i<qs;i++) {
                $rootScope.quantities.push(i);
            }
        },
    }
});