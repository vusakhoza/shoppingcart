angular.module('app', [
    'app.controllers',
    'app.services',
    'ngRoute',
    'ngSanitize',
    'ngAnimate',
])

.config(['$routeProvider', function($routeProvider) {
    $routeProvider
    .when("/cart", {
        templateUrl: "cart.html",
        controller: "cartcontroller",
    })
    .when("/home", {
        templateUrl: "home.html",
        controller: "homecontroller",
    })
    .when("/", {
        templateUrl: "signin.html",
        controller: "controller",
    })
    .otherwise({ redirectTo: '/' });


}]);