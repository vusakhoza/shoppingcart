angular.module('app.controllers', [])

.controller('cartcontroller', function($http, $scope, global, $rootScope, $routeParams, homeservice) {

    global.init();

    homeservice.getCart($rootScope);

    $scope.remove = function(id) {
        homeservice.remove($rootScope, id);
    }

    $scope.update = function(id) {
        var q = $( "#"+id+"_quantity" ).val();
        console.log(id+":"+q);
        homeservice.add($rootScope, $scope, id, q, false);
    }

})

.controller('homecontroller', function($http, global, $scope, $rootScope, $routeParams, homeservice) {
    
    global.init();

    homeservice.getProducts($scope);
    homeservice.getCart($rootScope);

    $scope.add = function(id) {
        var q = $( "#"+id+"_quantity" ).val();
        //console.log(id+":"+q);
        homeservice.add($rootScope, $scope, id, q, true);
    }

})

.controller('controller', function($http, $location, $scope, $rootScope, $routeParams) {
    
    $rootScope.nav = false;

    $scope.signin = function() {
        $scope.error = "";
        if($scope.user == "admin" && $scope.pass == "admin") {
            $location.path("/home");
        }else {
            $scope.error = "Wrong username/password please try again.";
        }
    }

});
