<?php
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: POST, GET, OPTIONS');

$db = new mysqli('localhost', 'root', 'Lmbo_8989', 'shoppingcart');
if($db->connect_errno > 0){
    die('Unable to connect to database [' . $db->connect_error . ']');
}


if( isset( $_POST["action"] ) ) {
    $action = $db->real_escape_string($_POST["action"]);
    switch( $action ) {

        case "getcart":
            $cart = array();
            $sql = "SELECT cart.product_id, cart.quantity, products.title, products.price, 
            products.description, products.id, products.image FROM cart INNER JOIN products ON 
            cart.product_id=products.id";
            if(!$re = $db->query($sql)){
                die('There was an error running the query [' . $db->error . ']');
            }

            while($ro = $re->fetch_assoc()) {
                $cart[] = $ro;
            }

            echo json_encode($cart);
            break;

        case "products":
            $products = array();

            $re = $db->query("SELECT*FROM products");
            while($ro = $re->fetch_assoc()) {
                $cq = $db->query("SELECT*FROM cart WHERE product_id = '".$ro["id"]."'");
                $cr = $cq->fetch_assoc();
                if( $cq->num_rows > 0 ) $ro["incart"] = true;
                $ro["quantity"] = $cr["quantity"];
                $products[] = $ro;
            }
            echo json_encode($products);
            break;

        case "remove":

            $product_id = $_POST["product_id"];
            $cart = array();

            $ce = $db->query("DELETE FROM cart WHERE product_id = '$product_id'");

            $re = $db->query("SELECT cart.product_id, cart.quantity, products.title, products.price, 
            products.description, products.id, products.image FROM cart INNER JOIN products ON 
            cart.product_id=products.id");

            while($ro = $re->fetch_assoc()) {
                $cart[] = $ro;
            }

            echo json_encode($cart);
            break;

        case "addtocart":

            $product_id = $_POST["product_id"];
            $quantity = $_POST["quantity"];
            $bool = $_POST["bool"];
            $cart = array();

            $ce = $db->query("SELECT*FROM cart WHERE product_id = '$product_id'");
            $co = $ce->fetch_assoc();

            if( $ce->num_rows == 0 )
                $db->query("INSERT INTO cart (product_id, quantity) VALUES ('$product_id', '$quantity')");
            else{
                //if on home page add previous quantity to chosen quantity
                if($bool == "true") $q = $co['quantity'] + $quantity;
                //else if on cart page adjust to indicated amount
                else $q = $quantity;

                $db->query("UPDATE cart SET quantity = '$q' WHERE product_id = '$product_id'");
            }
            $re = $db->query("SELECT cart.product_id, cart.quantity, products.title, products.price, 
            products.description, products.id, products.image FROM cart INNER JOIN products ON 
            cart.product_id=products.id");

            while($ro = $re->fetch_assoc()) {
                $cart[] = $ro;
            }

            echo json_encode($cart);
            break;

        default:
            echo "No action";
            break;
    }

}else {
    echo "Bad Request";
}

?>